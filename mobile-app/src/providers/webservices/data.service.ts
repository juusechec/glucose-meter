import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataModel } from './data.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataService {

  private url:string = 'http://192.168.0.18:8000/datos_compuestos/';

  constructor(
    private http: HttpClient
  ) {}

  public save(data: DataModel): Observable<any> {
    return this.http.post(this.url, data);
  }
}
