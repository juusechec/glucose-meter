import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CuestionarioModel } from './cuestionario.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CuestionarioService {

  private url:string = 'http://192.168.0.18:8000/cuestionarios/';

  constructor(
    private http: HttpClient
  ) {}

  public save(cuestionario: CuestionarioModel): Observable<CuestionarioModel> {
    return this.http.post<CuestionarioModel>(this.url, cuestionario);
  }
}
