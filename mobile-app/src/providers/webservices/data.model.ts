export interface DataModel {
    id_cuestionario: number,
    fecha_hora: string,
    temperatura_dedo: number,
    temperatura_externa: number,
    senal_transmision: Array<number>,
    senal_reflexion: Array<number>,
}