import { Injectable } from '@angular/core';

/*
  Generated class for the DeviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DeviceProvider {

  private device: any;

  constructor() {
    console.log('Hello DeviceProvider Provider');
  }

  setDevice (device) {
    this.device = device;
  }

  getDevice () {
    return this.device;
  }

  clearDevice () {
    this.device = undefined;
  }

}
