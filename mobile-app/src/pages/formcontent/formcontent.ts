import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform, NavParams, ViewController, ToastController, NavController } from 'ionic-angular';
import { FormGroup, FormControl } from '@angular/forms';
import { CuestionarioService } from '../../providers/webservices/cuestionario.service';
import { CuestionarioModel } from '../../providers/webservices/cuestionario.model';

// https://github.com/ionic-team/ionic-preview-app/blob/master/src/pages/modals/basic/modal-content.html
@Component({
    selector: 'formcontent',
    templateUrl: 'formcontent.html'
})
export class FormContentPage {

    constructor(
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public platform: Platform,
        public params: NavParams,
        private toastCtrl: ToastController,
        private cuestionarioService: CuestionarioService,
        private storage: Storage,
    ) {

    }

    formdata: FormGroup;

    showQuestions: boolean = false;
    male: boolean = false;
    female: boolean = false;

    ngOnInit() {
        this.formdata = new FormGroup({
            edad: new FormControl(''),
            estatura: new FormControl(''),
            peso: new FormControl(''),
            cintura: new FormControl(''),
            sexo: new FormControl(''),
            numero_hijos: new FormControl(''),
            diabetes_gestacional: new FormControl(''),
            diabetes: new FormControl(''),
            cigarrillos_semana: new FormControl(''),
            bebida_azucarada_semana: new FormControl(''),
            deporte_semana: new FormControl(''),
            vasos_agua_semana: new FormControl(''),
            sed: new FormControl(''),
            miccion_semana: new FormControl(''),
            antecedentes_familiares_glucosa: new FormControl(''),
        });
        // setTimeout(() => {
        //     this.removeComponent(this.navCtrl, FormContentPage);
        // }, 5000);
    }

    dismiss() {
        this.viewCtrl.dismiss(undefined);
    }

    submitForm(form) {
        form.diabetes_gestacional = (form.diabetes_gestacional === 'true');
        form.diabetes = (form.diabetes === 'true');
        form.sed = (form.sed === 'true');
        form.antecedentes_familiares_glucosa = (form.antecedentes_familiares_glucosa === 'true');
        form.vasos_agua_semana = +form.vasos_agua_semana * 7;
        form.miccion_semana = +form.miccion_semana * 7;
        if (this.male) {
            form.numero_hijos = 0;
            form.diabetes_gestacional = false;
        }
        form['fecha_hora'] = (new Date()).toISOString();
        form['imc'] = parseFloat(form.peso) / Math.pow(parseFloat(form.estatura), 2);
        console.log('CuestionarioModel request', form)
        this.cuestionarioService.save(form).subscribe((response: CuestionarioModel) => {
            console.log('CuestionarioModel response', response);
            this.showToast('¡Parámetros Guardados!');
            this.showToast('ID Cuestionario: ' + response.id, undefined, 5000, 'top');
            this.showToast('ID Cuestionario: ' + response.id, undefined, 5000, 'middle');
            this.showToast('ID Cuestionario: ' + response.id, undefined, 5000, 'bottom');
            this.storage.set('id', response.id).then(() => {
                this.navCtrl.parent.select(1); // DetailPage
                this.dismiss();
                // this.removeComponent(this.navCtrl, FormContentPage);
            });
        }, (err) => {
            this.showToast('Error enviando parámetros, intente nuevamente: ' + JSON.stringify(err));
            this.showToast('El dispositivo no pudo ser encendido, error: ' + JSON.stringify(err.error) + '.', undefined, 5000, 'middle');
        });
    }

    changeSex(sex) {
        sex = +sex;
        if (sex === 0) {
            this.male = true;
            this.female = false;
        } else if (sex === 1) {
            this.male = false;
            this.female = true;
        }
        this.showQuestions = true;
    }

    showToast(message: string, duration: number = 5000, timeout: number = 0, position: string = 'top') {
        let toast = this.toastCtrl.create({
            message: message,
            position: position,
            duration: duration
        });
        setTimeout(() => {
            toast.present();
        }, timeout);
    }

    removeComponent(nav, item) {
        console.log('nav', nav);
        const views = nav.getViews();
        for (var i = views.length; i--;) {
            const view = views[i];
            const component = view.component;
            if (component === item) {
                nav.removeView(view);
            }
        }
    }
}