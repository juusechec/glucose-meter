import { Component, NgZone } from '@angular/core';
import { NavController, ToastController, ModalController } from 'ionic-angular';
import { Diagnostic } from '@ionic-native/diagnostic';
import { BLE } from '@ionic-native/ble';
import { ModalContentPage } from '../modal/modal';
import { FormContentPage } from '../formcontent/formcontent';
import { DeviceProvider } from '../../providers/device/device';
import { DeviceModel } from '../../models/device.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  deviceConnected: boolean;
  devices: DeviceModel[] = [];
  statusMessage: string;
  lockRequest = false;

  constructor(
    public navCtrl: NavController,
    private ble: BLE,
    private ngZone: NgZone,
    private toastCtrl: ToastController,
    public modalCtrl: ModalController,
    private diagnostic: Diagnostic,
    private deviceProvider: DeviceProvider
  ) {
    // https://github.com/don/ionic-ble-examples/blob/master/connect/src/pages/home/home.ts
  }

  // https://ionicframework.com/docs/api/navigation/NavController/
  ionViewDidEnter() {
    console.log('ionViewDidEnter');
    // this.scan();
    this.deviceConnected = !!this.deviceProvider.getDevice();
  }

  scan() {
    this.setStatus('Scanning for Bluetooth LE Devices');
    this.devices = [];  // clear list
    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device),
      error => this.scanError(error)
    );

    setTimeout(this.setStatus.bind(this), 5000, 'Scan complete');
  }

  onDeviceDiscovered(device) {
    console.log('Discovered ' + JSON.stringify(device, null, 2));
    this.ngZone.run(() => {
      this.devices.push(device);
    });
  }

  // If location permission is denied, you'll end up here
  scanError(error) {
    this.setStatus('Error: ' + error);
    this.showToast('Error scanning for Bluetooth low energy devices.');
  }

  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }

  deviceSelected(device) {
    console.log(JSON.stringify(device) + ' selected');
    this.deviceProvider.setDevice(device);
    // this.navCtrl.push(DetailPage, {
    //   device: device
    // });
    this.navCtrl.parent.select(1); // DetailPage
  }

  openModal(retryCount = 0) {
    console.log('this.devices', this.devices, retryCount);
    if (this.devices.length == 0) {
      this.showToast('No se han encontrado dispositivos.\nReintentando...', 4000);
      this.scan();
      const retryMax = 2;
      if (retryCount < retryMax) {
        retryCount++;
        setTimeout(() => {
          this.openModal(retryCount);
        }, 5000);
      } else {
        this.showToast('No hay dispositivos cerca a ti.');
        this.lockRequest = false;
      }
      return;
    }
    this.lockRequest = false;
    this.showToast('¡Escaneo completo!', 1000, 100);
    let modalContentPage = this.modalCtrl.create(ModalContentPage, { devices: this.devices });
    modalContentPage.onDidDismiss(dev => {
      console.log('Seleccionado: ', dev);
      if (dev) {
        this.showToast('Conectado al dispositivo: ' + ( dev.name  + ' ' || '' + dev.id ) + '.', 2000);
        // this.showToast('Ve a la pestaña de DATOS.', 2000, 1000);
        this.deviceSelected(dev);
      }
    });
    modalContentPage.present();
  }

  showToast(message: string, duration: number = 5000, timeout: number = 0, position: string = 'top') {
    let toast = this.toastCtrl.create({
      message: message,
      position: position,
      duration: duration
    });
    setTimeout(() => {
      toast.present();
    }, timeout);
  }

  requestPermission() {
    if (this.lockRequest) {
      return;
    }
    this.lockRequest = true;
    let successCallback = (isAvailable) => {
      console.log('Is available? ' + isAvailable);
      if (isAvailable) {
        this.selectDevice();
      } else {
        this.showToast('El dispositivo Bluetooth no está disponible.\nEncendiendo automáticamente.', 2000);
        this.diagnostic.setBluetoothState(true).then(() => {
          console.log('Encendido');
          this.showToast('El dispositivo fue encendido automáticamente de manera exitosa.', 2000, 1000, 'middle');
          setTimeout(() => {
            this.selectDevice();
          }, 1000);
        }, error => {
          console.log('Error:' + error);
          this.showToast('El dispositivo no pudo ser encendido, error: ' + error + '.', undefined, undefined, 'middle');
          this.lockRequest = false;
        });
      }
    };
    let errorCallback = (e) => console.error(e);

    this.diagnostic.isBluetoothAvailable().then(successCallback, errorCallback);
  }

  selectDevice() {
    this.scan();
    this.showToast('Escaneando dispositivos...', 3000, 1000);
    setTimeout(() => {
      this.openModal();
    }, 5100);
  }

  showForm() {
    // const modalContentPage = this.modalCtrl.create(FormContentPage);
    // modalContentPage.onDidDismiss(dev => {
    //   console.log('Terminado Formulario: ', dev);
    // });
    // modalContentPage.present();
    // setTimeout(() => {
    //   this.navCtrl.push(FormContentPage);
    // }, 2000);
    this.navCtrl.push(FormContentPage);
  }

}
