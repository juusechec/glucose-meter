import { Component, NgZone } from '@angular/core';
import { ToastController, ModalController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { BLE } from '@ionic-native/ble';
import { DeviceProvider } from '../../providers/device/device';
import { DeviceModel } from '../../models/device.model';
import { BackgroundMode } from '@ionic-native/background-mode';
import { FormContentPage } from '../formcontent/formcontent';
import { DataService } from '../../providers/webservices/data.service';
import { Storage } from '@ionic/storage';

// Bluetooth UUIDs
const SERVICE_UUID = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E'; // UART service UUID
const CHARACTERISTIC_UUID_TX_CH1 = '6E400002-B5A3-F393-E0A9-E50E24DCCA9E';
const CHARACTERISTIC_UUID_RX_CH1 = '6E400003-B5A3-F393-E0A9-E50E24DCCA9E';
// const CHARACTERISTIC_UUID_TX_CH2 = '6E400004-B5A3-F393-E0A9-E50E24DCCA9E';
// const CHARACTERISTIC_UUID_RX_CH2 = '6E400005-B5A3-F393-E0A9-E50E24DCCA9E';

@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html'
})
export class DetailPage {

  customRootPath: string;
  customPath: string;
  customPathName = 'glucose-meter';
  customFilePath: string;
  customFileName = 'test.csv';
  device: DeviceModel;
  peripheral: any = {};
  isConnected = false;
  recievedValue1: any;
  recievedValue2: any;
  data: any;
  instructions: string;
  tempFinger: number;
  tempExternal: number;
  percent: number;
  dataTypeTransmision = 'tx';
  dataTypeReflexion = 'rx';
  currentDataType: string;
  glucose: number;
  enableResendGlucose:boolean = false;
  lastDataModel: any;

  constructor(
    private file: File,
    private toastCtrl: ToastController,
    private deviceProvider: DeviceProvider,
    private ble: BLE,
    private ngZone: NgZone,
    private backgroundMode: BackgroundMode,
    private modalCtrl: ModalController,
    private dataService: DataService,
    private storage: Storage,
  ) {
    // https://github.com/don/ionic-ble-examples/blob/master/connect/src/pages/detail/detail.ts
    this.customRootPath = this.file.externalRootDirectory;
    this.customPath = this.customRootPath + this.customPathName;
    this.customFilePath = this.customPath + this.customFileName;
    this.createDir();
    this.createFile('channel1.csv');
    // this.createFile('channel2.csv');
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter');
    this.device = this.deviceProvider.getDevice();
    if (this.device) {
      this.connect();
    }
    this.cleanData();
  }

  createDir() {
    // file.dataDirectory
    this.file.checkDir(this.customRootPath, this.customPathName)
    .then(_ => {
      console.log('Directory exists', this.customPath);
    })
    .catch(err => {
      console.log('Directory doesn\'t exist', this.customPath, err);
      this.file.createDir(this.customRootPath, this.customPathName, false)
      .then(_ => {
        console.log('Directory CREATED', this.customPath);
      })
      .catch(err => {
        console.log('Error creating directory', err);
        this.showToast('Error creating directory: ' + JSON.stringify(err));
      });
    });
  }

  saveFile() {
    this.file.createFile(this.customPath, this.customFileName, true)
    .then(_ => {
      this.file.writeFile(this.customPath, this.customFileName, 'value1,value2,value3\r\n', {append: true})
      .then(_ => {
        console.log('Adding Text', this.customFilePath);
        this.showToast('Data writed successfully!');
      })
      .catch(err => {
        console.log('Error writing file', err);
        this.showToast('Error writing file: ' + JSON.stringify(err));
      });;
    })
    .catch(err => {
      console.log('Error creating file', err);
      this.showToast('Error creating file: ' + JSON.stringify(err));
    });
  }

  writeFile(filename: string, value: string) {
    this.file.writeFile(this.customPath, filename, value, {append: true})
    .then(_ => {
      console.log('Adding Text', this.customPath + '/' + filename);
      // this.showToast('Data writed successfully!');
    })
    .catch(err => {
      console.log('Error writing file', err);
      this.showToast('Error writing file: ' + JSON.stringify(err));
    });
  }

  saveData(filename: string, value: string) {
    this.file.checkFile(this.customPath + '/', filename).then(() => {
      console.log('File exists', this.customPath + '/' + filename);
      this.writeFile(filename, value);
    }).catch(err => {
      console.log('File doesn\'t exists', this.customPath + '/' + filename, err);
      this.file.createFile(this.customPath, filename, true)
      .then(_ => {
        console.log('File created succesfully.');
        this.showToast('File created succesfully.');
        this.writeFile(filename, value);
      })
      .catch(err => {
        console.log('Error creating file', err);
        this.showToast('Error creating file: ' + JSON.stringify(err));
      });
    });
  }

  createFile(filename: string) {
    this.file.checkFile(this.customPath + '/', filename).then(() => {
      console.log('File exists', this.customPath + '/' + filename);
    }).catch(err => {
      console.log('File doesn\'t exists', this.customPath + '/' + filename, err);
      this.file.createFile(this.customPath, filename, true)
      .then(_ => {
        console.log('File created succesfully.');
        this.showToast('File created succesfully.');
      })
      .catch(err => {
        console.log('Error creating file', err);
        this.showToast('Error creating file: ' + JSON.stringify(err));
      });
    });
  }

  onConnected(peripheral) {
    this.showToast('Dispositivo conectado.');
    this.cleanData();
    this.backgroundMode.enable();
    this.ngZone.run(() => {
      this.peripheral = peripheral;
    });
    this.readValueOfBLE();
    this.deviceProvider.setDevice(this.device);
    this.isConnected = true;
  }

  onDeviceDisconnected(peripheral) {
    this.showToast('El dispositivo se ha desconectado inesperadamente.');
    this.ngZone.run(() => {
      this.deviceProvider.clearDevice();
    });
    this.isConnected = false;
    this.peripheral = undefined;
    this.backgroundMode.disable();
  }

  // Disconnect peripheral when leaving the page
  ionViewWillLeave() {
    console.log('ionViewWillLeave disconnecting Bluetooth');
    this.ble.disconnect(this.peripheral.id).then(
      () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
      () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
    )
  }

  connect() {
    this.showToast('Connecting to ' + (this.device.name || this.device.id));
    this.isConnected = true; // Prevent button press, only if fails
    this.peripheral = undefined;

    this.ble.connect(this.device.id).subscribe(
      peripheral => this.onConnected(peripheral),
      peripheral => this.onDeviceDisconnected(peripheral)
    );
    console.log('device:', this.device);
  }

  showToast(message: string, duration: number = 5000, timeout: number = 0, position: string = 'top') {
    let toast = this.toastCtrl.create({
      message: message,
      position: position,
      duration: duration
    });
    setTimeout(() => {
      toast.present();
    }, timeout);
  }

  // ASCII only
  stringToBytes(string) {
    var array = new Uint8Array(string.length);
    for (var i = 0, l = string.length; i < l; i++) {
        array[i] = string.charCodeAt(i);
    }
    return array.buffer;
  }

  // ASCII only
  bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  }

  // ASCII only
  bytesToNumbersArray(buffer) {
    const uint8Array = new Uint8Array(buffer);
    const array = Array.from(uint8Array)
    return array;
  }

  readValueOfBLE() {

    // Subscribe for notifications when the value changes
    // https://github.com/don/ionic-ble-examples/blob/master/thermometer/src/pages/detail/detail.ts
    this.ngZone.run(() => { 
      this.ble.startNotification(this.peripheral.id, SERVICE_UUID, CHARACTERISTIC_UUID_RX_CH1).subscribe(
        data => this.onVoltageChange1(data),
        err => console.log('Error in subscribe 1:', err)
      );
    });

    // this.ble.startNotification(this.peripheral.id, SERVICE_UUID, CHARACTERISTIC_UUID_RX_CH2).subscribe(
    //   data => this.onVoltageChange2(data),
    //   err => console.log('Error in subscribe 2:', err)
    // );
    // this.ngZone.run(() => { 
    //   this.ble.read(this.peripheral.id, SERVICE_UUID, CHARACTERISTIC_UUID_RX_CH1).then(
    //     buffer => {
    //       console.log('Llegue');
    //       // let data = new Uint8Array(buffer);
    //       console.log('Characteristic... ', buffer);
    //       // this.onVoltageChange1(buffer);
    //       // this.ngZone.run(() => {
    //       //     this.recievedValue = data;
    //       // });
    //     },
    //     err => {
    //       console.log('Error in BLE read1:', err);
    //     }
    //   ).catch(err => {
    //     console.log('Error in BLE read2:', err);
    //   });
    // });
  }

  onVoltageChange1(buffer:ArrayBuffer) {
    this.ngZone.run(() => {
      // console.log('buffer1', buffer);
      //const voltage = this.bytesToString(buffer);
      //console.log('voltage:', voltage);
      // Temperature is a 4 byte floating point value
      // var data = new Float32Array(buffer);
      //console.log(data[0]);
      const data = this.bytesToStringArray(buffer);
      // console.log('data:', data);
      this.processDataFrames(data);
      // rowValue += '\r\n';
      // console.log('rowValue channel1', rowValue);
      // this.writeFile('channel1.csv', rowValue);
      // // this.recievedValue = voltage;
      // this.recievedValue1 = rowValue;
      let recievedBuffer = new Uint8Array([0]).buffer;
      this.ble.write(this.peripheral.id, SERVICE_UUID, CHARACTERISTIC_UUID_TX_CH1, recievedBuffer).then(
        () => { },
        e => { console.log('Send Error: ' + e); }
      );
    });
  }

  bytesToStringCut(buffer): string {
    let numberArray = this.bytesToNumbersArray(buffer);
    // (<any>window).numberArray = numberArray;
    let rowValue = '';
    for (let i=0; i < numberArray.length; i++) {
      let num = numberArray[i];
      if (i < 8) {
        rowValue += String.fromCharCode(num);
      }
    }
    rowValue = rowValue.slice(0, rowValue.length-1);
    return rowValue;
  }

  bytesToStringArray(buffer): string {
    let numberArray = this.bytesToNumbersArray(buffer);
    // (<any>window).numberArray = numberArray;
    let rowValue = '';
    for (let i=0; i < numberArray.length; i++) {
      let num = numberArray[i];
      rowValue += String.fromCharCode(num);
    }
    return rowValue;
  }

  onVoltageChange2(buffer:ArrayBuffer) {
    console.log('buffer2', buffer);
    let numberArray = this.bytesToNumbersArray(buffer);
    let rowValue = '';
    for (let num of numberArray) {
      rowValue += num + ',';
    }
    rowValue = rowValue.slice(0, rowValue.length-1);
    rowValue += '\r\n';
    this.ngZone.run(() => {
      this.writeFile('channel2.csv', rowValue);
      this.recievedValue2 = numberArray;
    });
  }

  processDataFrames(data: string) {
    if (data.startsWith('BOTON0')) {
      this.instructions = 'Tomando datos, por favor espere...';
      this.resetData();
      this.showToast('Iniciando conteo de segundos.', 2000, null, 'bottom');
      setTimeout(() => {
        this.showToast('Han pasado 20 segundos.', 5000, null, 'bottom');
      }, 20000);
    } else if (data.startsWith('TMP102')) {
      this.instructions = 'Transfiriendo datos de Transmisión y Reflexión...';
      this.tempFinger = +data.substring(10, 19);
      console.log('TMP12', data, this.tempFinger);
      this.percent = undefined;
    } else if (data.startsWith('LM35')) {
      this.instructions = 'Transfiriendo datos de Transmisión y Reflexión...<br>Por favor espere.';
      this.tempExternal = +data.substring(10, 19);
      console.log('LM35', data, this.tempExternal);
    } else if (data.startsWith('ADC0')) {
      this.instructions = 'Transfiriendo datos de Transmisión...';
      this.currentDataType = this.dataTypeTransmision;
    } else if (data.startsWith('ADC1')) {
      this.instructions = 'Transfiriendo datos de Reflexión...';
      this.currentDataType = this.dataTypeReflexion;
    } else if (data.startsWith('END')) {
      this.percent = undefined;
      this.instructions = 'Esperando cálculo de glucosa...';
      console.log('END Finish Data', this.data);
      this.storage.get('id').then((id_cuestionario: string) => {
        console.log('id_cuestionario', id_cuestionario);
        const dataModel = {
          id_cuestionario: +id_cuestionario,
          fecha_hora: (new Date()).toISOString(),
          temperatura_dedo: this.tempFinger,
          temperatura_externa: this.tempExternal,
          senal_transmision: this.data[this.dataTypeTransmision],
          senal_reflexion: this.data[this.dataTypeReflexion],
        };
        console.log('DataModel request', dataModel);
        this.lastDataModel = dataModel;
        this.sendGlucose();
      });
    } else {
      for (let i = 0; i < 20; i += 4) {
        const final = data.substring(i, i + 4);
        this.data[this.currentDataType].push(parseInt(final));
        const length = this.data[this.currentDataType].length;
        if (length % 500) {
          this.percent = parseFloat(((length / 2500) * 100).toFixed(1));
        }
      }
    }
  }

  showForm() {
    let modalContentPage = this.modalCtrl.create(FormContentPage);
    modalContentPage.onDidDismiss(() => {
      console.log('Cerrado');
    });
    modalContentPage.present();
  }

  cleanData() {
    this.instructions = 'Presione el botón 0 para iniciar';
    this.percent = undefined;
    this.glucose = undefined;
  }

  resetData() {
    this.data = {
      [this.dataTypeTransmision]: [],
      [this.dataTypeReflexion]: []
    };
  }

  sendGlucose() {
    this.dataService.save(this.lastDataModel).subscribe(response => {
      this.enableResendGlucose = false;
      console.log('DataModel response', response);
      this.glucose = response.glucosa;
      this.instructions = 'Se terminó el resultado.';
      this.showToast('¡Se han guardado los datos exitosamente!');
    }, err => {
      this.enableResendGlucose = true;
      console.log('Error dataModel: ', err);
      this.showToast('Error enviando parámetros, intente nuevamente: ' + JSON.stringify(err));
    });
  }

}
