import { Component, ViewChild } from '@angular/core';
import { Tabs } from 'ionic-angular';
import { DetailPage } from '../detail/detail';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  @ViewChild('tabs') tabRef: Tabs;

  tab1Root = HomePage;
  tab2Root = DetailPage;
  tab3Root = ContactPage;

  constructor(
  ) {
    // https://ionicframework.com/docs/api/components/tabs/Tabs/
  }

  ionViewDidEnter() {
  }
}
  