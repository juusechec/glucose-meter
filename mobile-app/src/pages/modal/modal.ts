import { Component } from '@angular/core';
import { Platform, NavParams, ViewController } from 'ionic-angular';
import { DeviceModel } from '../../models/device.model';

// https://github.com/ionic-team/ionic-preview-app/blob/master/src/pages/modals/basic/modal-content.html
@Component({
    selector: 'modal',
    templateUrl: 'modal.html'
})
export class ModalContentPage {

    recommendeDevice: DeviceModel;
    devices: Array<DeviceModel>;

    constructor(
        public viewCtrl: ViewController,
        public platform: Platform,
        public params: NavParams
    ) {
        this.devices = this.params.get('devices') as Array<DeviceModel>;
        for (let dev of this.devices) {
            // Only the first coincidence
            if (dev.name && dev.name.toUpperCase().includes('ESP32')) {
                this.recommendeDevice = dev;
                this.devices = this.devices.filter((item) => item != dev);
                break;
            }
        }
    }

    dismiss() {
        this.viewCtrl.dismiss(undefined);
    }

    choose(item) {
        console.log('item:', item);
        this.viewCtrl.dismiss(item);
    }

}