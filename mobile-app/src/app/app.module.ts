import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { DetailPage } from '../pages/detail/detail';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ModalContentPage } from '../pages/modal/modal';
import { FormContentPage } from '../pages/formcontent/formcontent';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { BLE } from '@ionic-native/ble';

import { File } from '@ionic-native/file';

import { IonicStorageModule } from '@ionic/storage';

import { Diagnostic } from '@ionic-native/diagnostic';
import { BackgroundMode } from '@ionic-native/background-mode';
import { DeviceProvider } from '../providers/device/device';
import { HttpClientModule } from '@angular/common/http';
import { CuestionarioService } from '../providers/webservices/cuestionario.service';
import { DataService } from '../providers/webservices/data.service';

@NgModule({
  declarations: [
    MyApp,
    DetailPage,
    ContactPage,
    HomePage,
    TabsPage,
    ModalContentPage,
    FormContentPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DetailPage,
    ContactPage,
    HomePage,
    TabsPage,
    ModalContentPage,
    FormContentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BLE,
    File,
    Diagnostic,
    BackgroundMode,
    DeviceProvider,
    CuestionarioService,
    DataService
  ]
})
export class AppModule {}
