#!/usr/bin/env python3
##############
## Script listens to serial port and writes contents into a file
##############
## requires pySerial to be installed
## python3 -m pip install pyserial
import os
import serial
import datetime
from termcolor import colored

import subprocess

serial_port = '/dev/ttyUSB0'
baud_rate = 115200 #In arduino, Serial.begin(baud_rate)
directory = 'samples'
if not os.path.exists(directory):
    os.makedirs(directory)
os.chdir(directory)

def getfilename():
    datestamp = '{0:%Y-%m-%d__%H_%M_%S}'.format(datetime.datetime.now())
    return 'output_{}.txt'.format(datestamp)

print('Para iniciar cada muestreo usa el pulsador en el pin 0 del microcontrolador.')
ser = serial.Serial(serial_port, baud_rate)
enable_write = False
output_file = None # initial value
subprocess_output = None
try:
    while True:
        line = ser.readline()
        line = line.decode('utf-8') #ser.readline returns a binary, convert to string
        print(line, end='') # uncomment this line if you want to see line in console too
        if line.startswith('#Iniciando...'):
            output_file = open(getfilename(), 'w+')
            if subprocess_output is not None:
                subprocess_output.kill()
            enable_write = True
        if line.startswith('#Finalizado!!!'):
            output_file.close()
            print(colored('El archivo ha sido guardado exitosamente con el nombre: {}'.format(output_file.name), 'red'))
            filepath = os.path.realpath(output_file.name)
            subprocess_output = subprocess.Popen(['/usr/bin/env', 'python3', '../plotdata.py', filepath], stdout=subprocess.PIPE)
            enable_write = False
        if enable_write and line[0].isalnum():
            output_file.write(line)
except Exception as e:
    print(e)
finally:
    if output_file is not None:
        output_file.close()