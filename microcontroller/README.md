# glucose-meter

This a source code for glucose-meter based on spectroscopy AI analysis.

With [ESP32 microcontroller](https://www.espressif.com/en/products/hardware/esp32/overview) we sense levels of diodes in near-infrared spectrum and send to Smartphone for exhaustive analysis.