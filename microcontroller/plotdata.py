#!/usr/bin/env python3
import sys

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.fftpack
import scipy.signal

filename = None
if len(sys.argv) < 2:
    sys.exit()
filename = sys.argv[1]

df = pd.read_csv(filename, sep=',', skiprows=4)

N = 2500
T = 0.008
t = np.linspace(0.0, N*T, N)

y = df['adc0'].values

fig, axs = plt.subplots(nrows=1, ncols=2, sharex=True)
fig.suptitle('Medidas')
ax = axs[0]
ax.plot(t, y)
ax.set_title('ADC0')

y = y - np.mean(y) # remove DC
yf = scipy.fftpack.fft(y)
xf = np.linspace(0.0, 1.0/(2.0*T), N/2)
yfmag = 2.0/N * np.abs(yf[:N//2])

ax = axs[1]
ax.set_title('FFT')
ax.plot(xf, yfmag)

y = df['adc1'].values

fig, axs = plt.subplots(nrows=1, ncols=2, sharex=True)
fig.suptitle('Medidas')
ax = axs[0]
ax.plot(t, y)
ax.set_title('ADC1')

y = y - np.mean(y) # remove DC
yf = scipy.fftpack.fft(y)
xf = np.linspace(0.0, 1.0/(2.0*T), N/2)
yfmag = 2.0/N * np.abs(yf[:N//2])

ax = axs[1]
ax.set_title('FFT')
ax.plot(xf, yfmag)

plt.show()
