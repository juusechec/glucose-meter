#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

BLECharacteristic *pCharacteristic1;
bool deviceConnected = false;
bool dataReceived = false;
int channelValue1 = 0;
char message1[20]; // Un mensaje con 20 caracteres!
uint8_t * pointer1 = (uint8_t *)message1; // puntero del mensaje

int16_t indexOfSample = 0;
const int16_t samplesNumber = 10000;
int16_t channel0[samplesNumber];

int bufferIndex = 0;
// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX_CH1 "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX_CH1 "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      Serial.println("Connected");
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      Serial.println("Disconnected");
      deviceConnected = false;
    };
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();  
      if (rxValue.length() > 0) {
        dataReceived = true;
        Serial.print("Received Value: ");
        for (int i = 0; i < rxValue.length(); i++) {
          Serial.print((int)rxValue[i]);
        }
        Serial.println();
      }
    }
};

void setup() {
  Serial.begin(115200);
  
  BLEDevice::init("ESP32 UART Test"); // Give it a name

  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic1 = pService->createCharacteristic(
                      CHARACTERISTIC_UUID_TX_CH1,
                      BLECharacteristic::PROPERTY_NOTIFY
                    );
                      
  pCharacteristic1->addDescriptor(new BLE2902());


  BLECharacteristic *pCharacteristic1 = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID_RX_CH1,
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  pCharacteristic1->setCallbacks(new MyCallbacks());
  pService->start();

  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}


void convertIntToChar(int num, char * real) {
    char cstr[4] = {0, 0, 0, 0};
    itoa(num, cstr, 10);
    int j = 3;
    int k = 0;
    for(int i = 0; i < 4; i++) {
        if (cstr[j] == 0) {
            real[i] = '0';
            j--;
        } else {
            real[i] = cstr[k];
            k++;
        }
    }
}

void printData(char * arr) {
  for(int i = 0; i < 4; i++) {
      Serial.print((char)arr[i]);
  }
  Serial.println("");
}

void sendBuffer() {
  if (!deviceConnected) {
    delay(1000);
    Serial.println("Wait for device Connected");
    sendBuffer();
    return;
  }
  dataReceived = false;
  pCharacteristic1->setValue(pointer1, 20);
  pCharacteristic1->notify(); // Send the value to the app!
  Serial.print("*** Sent Value: ");
  for(size_t i = 0; i < 20; i++){
    Serial.print(message1[i]);
    Serial.print(',');
  }
  Serial.println("***");
  bufferIndex = 0;
  int timer = 0;
  while(!dataReceived) {
    if (timer > 10000000) {
      // espera 10 segundos para reintentar
      Serial.println("Retry!");
      sendBuffer();
      return;
    }
    delayMicroseconds(1);
    timer++;
  }
}

void writeToBuffer(char * arr) {
  for(size_t i = 0; i < 4; i++) {
    message1[bufferIndex] = arr[i];
    bufferIndex++;
  }
}

void readADCChannels(void) {
    channel0[indexOfSample] = channelValue1;
    indexOfSample++;
    channelValue1++;
    if (channelValue1 > 4095) {
      channelValue1 = 0;
    }
}

void inicioBLE() {
  Serial.println("Se comienza mensaje de inicio");
  for(int i = 0; i < 20; i++) {
      message1[i] = '8';
  }
  Serial.println("sendBuffer 0");
  sendBuffer();
  Serial.println("Se termina mensaje de inicio");
}

void finBLE() {
  Serial.println("Se comienza mensaje de finalización");
  for(int i = 0; i < 20; i++) {
      message1[i] = '9';
  }
  sendBuffer();
  Serial.println("Se termina mensaje de finalización");
}

void printBLEADC(void) {
    for(int16_t i = 0; i < samplesNumber; i++) {
        int16_t value0 = channel0[i];
        char arr[4];
        convertIntToChar(value0, arr);
        printData(arr);
        writeToBuffer(arr);
        if (bufferIndex == 20) {
          sendBuffer();
        }
    }
}

void loop() {
  while(indexOfSample < samplesNumber) {
      readADCChannels();
  }
  inicioBLE();
  printBLEADC();
  finBLE();
  delay(100000);
}
