#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <Adafruit_ADS1015.h>

Adafruit_ADS1015 ads1015; // I2C address 0x48

BLECharacteristic *pCharacteristic1;
BLECharacteristic *pCharacteristic2;
bool deviceConnected = false;
const int readPin1 = 32; // Use GPIO number. See ESP32 board pinouts // Is it an input pin by default?
const int readPin2 = 33; // Use GPIO number. See ESP32 board pinouts // Is it an input pin by default?
const int LED = 2; // Could be different depending on the dev board. I used the DOIT ESP32 dev board.

int16_t channelValue1 = 0;
char message1[20]; // Un mensaje con 20 caracteres!
// uint8_t *pointer1 = message1;
uint8_t * pointer1 = (uint8_t *)message1;

int bufferIndex = 0;

//std::string rxValue; // Could also make this a global var to access it in loop()

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX_CH1 "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX_CH1 "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_RX_CH2 "6E400004-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX_CH2 "6E400005-B5A3-F393-E0A9-E50E24DCCA9E"

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();

      if (rxValue.length() > 0) {
        Serial.println("*********");
        Serial.print("Received Value: ");

        for (int i = 0; i < rxValue.length(); i++) {
          Serial.print(rxValue[i]);
        }

        Serial.println();

        // Do stuff based on the command received from the app
        if (rxValue.find("A") != -1) { 
          Serial.print("Turning ON!");
          digitalWrite(LED, HIGH);
        }
        else if (rxValue.find("B") != -1) {
          Serial.print("Turning OFF!");
          digitalWrite(LED, LOW);
        }

        Serial.println();
        Serial.println("*********");
      }
    }
};

void setup() {
  Serial.begin(115200);

  pinMode(LED, OUTPUT);
  pinMode(readPin1, INPUT);
  analogSetWidth(12);

  // Create the BLE Device
  BLEDevice::init("ESP32 UART Test"); // Give it a name

  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic1 = pService->createCharacteristic(
                      CHARACTERISTIC_UUID_TX_CH1,
                      BLECharacteristic::PROPERTY_NOTIFY
                    );
                      
  pCharacteristic1->addDescriptor(new BLE2902());

  BLECharacteristic *pCharacteristic1 = pService->createCharacteristic(
                                         CHARACTERISTIC_UUID_RX_CH1,
                                         BLECharacteristic::PROPERTY_WRITE
                                       );

  pCharacteristic1->setCallbacks(new MyCallbacks());

  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");

  // ADC Config
  ads1015.setGain(GAIN_ONE); //        +/- 4.096V  1 cuenta = 2mV
  int sda = 14;
  int scl = 15;
  ads1015.begin(sda, scl);
}

void loop() {
  if (deviceConnected) {
    // Fabricate some arbitrary junk for now...
    //txValue = analogRead(readPin); // This could be an actual sensor reading!

    channelValue1 = ads1015.readADC_SingleEnded(0);
    Serial.printf("channelValue1: ");
    Serial.println(channelValue1);
    // float volt = (channelValue1*3.3)/1024.0; // 10 bits = 2^12 = 1024
    // float volt = (channelValue1*3.3)/4096.0; // 12 bits = 2^12 = 4096
    float volt = channelValue1*0.002F;
    Serial.printf("Voltage: ");
    Serial.println(volt, 6);
    // if (channelValue1 >= 4096) {
    //   channelValue1 = 0;
    // } else {
    //   channelValue1++;
    // }
    dtostrf(channelValue1, 7, 0, message1);

    if (bufferIndex == 20) {
      pCharacteristic1->setValue(pointer1, 20);
      pCharacteristic1->notify(); // Send the value to the app!
      Serial.print("*** Sent Value 1: ");
      for(size_t i = 0; i < 20; i++){
        Serial.print(message1[i]);
        Serial.print(',');
      }
      Serial.println("***");
    } else {
      message1[bufferIndex] = channelValue1;
      bufferIndex++;
    }

    
    // Let's convert the value to a char array:
    //char txString[8]; // make sure this is big enuffz
    //dtostrf(txValue, 1, 2, txString); // float_val, min_width, digits_after_decimal, char_buffer
    
//    pCharacteristic->setValue(&txValue, 1); // To send the integer value
//    pCharacteristic->setValue("Hello!"); // Sending a test message
   // pCharacteristic->setValue(txString);
    
    // pCharacteristic->notify(); // Send the value to the app!
    // Serial.print("*** Sent Value: ");
    // Serial.print(message);
    // Serial.println(" ***");

    // You can add the rxValue checks down here instead
    // if you set "rxValue" as a global var at the top!
    // Note you will have to delete "std::string" declaration
    // of "rxValue" in the callback function.
//    if (rxValue.find("A") != -1) { 
//      Serial.println("Turning ON!");
//      digitalWrite(LED, HIGH);
//    }
//    else if (rxValue.find("B") != -1) {
//      Serial.println("Turning OFF!");
//      digitalWrite(LED, LOW);
//    }
  }
  // delay(1000);
  //delay(1);
  delay(100);
}
