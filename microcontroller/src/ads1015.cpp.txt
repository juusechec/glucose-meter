#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_ADS1015.h>

Adafruit_ADS1015 ads1015;

void setup(void)
{
    Serial.begin(115200);
    Serial.println("Hello!");

    Serial.println("Getting single-ended readings from AIN0..3");
    Serial.println("ADC Range: +/- 6.144V (1 cuenta = 3mV)");
    // V positivos = 2 ^ 11 = 2048 cuentas
    // V negativos = 2 ^ 11 = 2048 cuentas
    // ads1015.setGain(GAIN_TWOTHIRDS);  +/- 6.144V  1 cuenta = 3mV (default)
    ads1015.setGain(GAIN_ONE); //        +/- 4.096V  1 cuenta = 2mV
    // ads1015.setGain(GAIN_TWO);        +/- 2.048V  1 cuenta = 1mV
    // ads1015.setGain(GAIN_FOUR);       +/- 1.024V  1 cuenta = 0.5mV
    // ads1015.setGain(GAIN_EIGHT);      +/- 0.512V  1 cuenta = 0.25mV
    // ads1015.setGain(GAIN_SIXTEEN);    +/- 0.256V  1 cuenta = 0.125mV
    int sda = 14;
    int scl = 15;
    ads1015.begin(sda, scl);
}

void loop(void)
{
    int16_t adc0, adc1, adc2, adc3;

    adc0 = ads1015.readADC_SingleEnded(0);
    // adc1 = ads1015.readADC_SingleEnded(1);
    // adc2 = ads1015.readADC_SingleEnded(2);
    // adc3 = ads1015.readADC_SingleEnded(3);
    Serial.print("AIN0: ");
    Serial.println(adc0);
    // Serial.print("AIN1: ");
    // Serial.println(adc1);
    // Serial.print("AIN2: ");
    // Serial.println(adc2);
    // Serial.print("AIN3: ");
    // Serial.println(adc3);
    // Serial.println(" ");

    // delay(100);
}