#include <Arduino.h>
#include <Wire.h>
#include <SparkFunTMP102.h>
#include <Adafruit_ADS1015.h>

/**
 * Button Globals
 */
#define BUTTON_PIN 0
volatile byte status = LOW;

/**
 * ADC Globals
 */
const int sdaPin = 14;
const int sclPin = 15;
Adafruit_ADS1015 ads1015; // Initialize ADC at I2C address 0x48
const int16_t samplesNumber = 2500;
int16_t indexOfSample = 0;
int16_t channel0[samplesNumber];
int16_t channel1[samplesNumber];
int16_t channel2[samplesNumber];
int16_t channel3[samplesNumber];
unsigned long initialTime;
unsigned long finalTime;
unsigned long diffTime;
unsigned long sampleTime;

/**
 * TMP Globals
 */
TMP102 sensor0(0x49); // Initialize temperature sensor at I2C address 0x49
float temperature;
const int temperatureSamples = 10;
const int temperatureTimeGap = 100; // ms

/**
 * LM35 Globals
 */
#define ANALOG_PIN 33
float temperatureLM35;
const int temperatureSamplesLM35 = 10;
const int temperatureTimeGapLM35 = 100; // ms

/**
 * Start program
 */

void blinkButton() {
    status = !status;
}

void configButton(void) {
    Serial.println("Config Button");
    pinMode(BUTTON_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), blinkButton, FALLING);
}

void configADC(void) {
    Serial.println("Config ADC");
    ads1015.setGain(GAIN_ONE); // +/- 4.096V 1 value = 2mV
    ads1015.begin(sdaPin, sclPin);
}

void configTMP(void) {
    Serial.println("Config TMP");
    // Join I2C bus
    sensor0.begin(sdaPin, sclPin);
    sensor0.setFault(0); // Trigger alarm immediately
    sensor0.setAlertPolarity(1); // Active HIGH
    sensor0.setAlertMode(0); // Comparator Mode.
    // set the Conversion Rate (how quickly the sensor gets a new reading)
    //0-3: 0:0.25Hz, 1:1Hz, 2:4Hz, 3:8Hz
    sensor0.setConversionRate(2);
    //set Extended Mode.
    //0:12-bit Temperature(-55C to +128C) 1:13-bit Temperature(-55C to +150C)
    sensor0.setExtendedMode(0);
    sensor0.setHighTempC(50); // set T_HIGH in alert of C
    sensor0.setLowTempC(0); // set T_LOW in alert of C
}

void configLM35(void) {
    Serial.println("Config LM35");
    analogReadResolution(10);
    pinMode(ANALOG_PIN, INPUT);
}

void setup(void) {
    Serial.begin(115200);
    Serial.println("¡Iniciando Configuración!");
    configADC();
    configTMP();
    configLM35();
    configButton();
}

void readADCChannels(void) {
    channel0[indexOfSample] = ads1015.readADC_SingleEnded(0);
    channel1[indexOfSample] = ads1015.readADC_SingleEnded(1);
    channel2[indexOfSample] = ads1015.readADC_SingleEnded(2);
    channel3[indexOfSample] = ads1015.readADC_SingleEnded(3);
    indexOfSample++;
}

void readTemperatureTMP(void) {
    temperature = 0;
    for(int i = 0; i < temperatureSamples; i++) {
        temperature += sensor0.readTempC();
        delay(temperatureTimeGap);
    }
    temperature = temperature / temperatureSamples;
}

void readTemperatureLM35(void) {
    temperatureLM35 = 0;
    for(int i = 0; i < temperatureSamplesLM35; i++) {
        temperatureLM35 += analogRead(ANALOG_PIN);
        delay(temperatureTimeGapLM35);
    }
    temperatureLM35 = temperatureLM35 / temperatureSamplesLM35;
    temperatureLM35 = (temperatureLM35 * 0.046454) + 2.030621;
}

void printSerialADC(void) {
    Serial.println("samplesNumber,sampleTime(uS),initialTimeStamp(uS),finalTimeStamp(uS),diffTimeStamp(uS)");
    Serial.print(samplesNumber);
    Serial.print(",");
    Serial.print(sampleTime);
    Serial.print(",");
    Serial.print(initialTime);
    Serial.print(",");
    Serial.print(finalTime);
    Serial.print(",");
    Serial.println(diffTime);
    Serial.println("adc0,adc1,adc2,adc3");
    for(int16_t i = 0; i < samplesNumber; i++) {
        int16_t value0 = channel0[i];
        int16_t value1 = channel1[i];
        int16_t value2 = channel2[i];
        int16_t value3 = channel3[i];
        Serial.print(value0);
        Serial.print(",");
        Serial.print(value1);
        Serial.print(",");
        Serial.print(value2);
        Serial.print(",");
        Serial.println(value3);
    }
}

void printTemperature(void) {
    Serial.println("fingerTemperatureCelsius(TMP102),externalTemperatureCelsius(LM35)");
    Serial.print(temperature, 4); // The on-chip 12-bit ADC offers resolutions down to 0.0625 °C
    Serial.print(",");
    Serial.print(temperatureLM35, 4);
    Serial.println("");
}

void loop(void) {
    if (status) {
        Serial.println("#Iniciando...");
        initialTime = micros();
        while(indexOfSample < samplesNumber) {
            readADCChannels();
        }
        indexOfSample = 0;
        finalTime = micros();
        diffTime = finalTime - initialTime;
        sampleTime = diffTime / samplesNumber;
        readTemperatureTMP();
        readTemperatureLM35();
        printTemperature();
        printSerialADC();
        status = false;
        Serial.println("#Finalizado!!!");
    }
}