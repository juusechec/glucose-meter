#!/usr/bin/env bash
if [ "$1" == "" ]
then
echo 'pailas'
exit 1   
fi
fullfile="$1"
filename=$(basename -- "$fullfile")
path=$(dirname "$fullfile")
# extension="${filename##*.}"
filename="${filename%.*}"
epstopdf --outfile="$path/$filename-eps-converted-to.pdf" "$fullfile"
