#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("logs/rmse.log", names=['rmse'])

df.hist(bins=50)
# df2 = df.query('rmse < 15')
# print(df2['rmse'].values)
# plt.plot(df2['rmse'].values)
plt.ylabel('Frecuecia del valor RMSE')
plt.xlabel('Raíz del error absoluto medio (RMSE)')
plt.title('')
plt.xlim(None, 200)
plt.show()


df = pd.read_csv("logs/rmse.log", names=['rmse'])

df.hist(bins=50)
# df2 = df.query('rmse < 15')
# print(df2['rmse'].values)
# plt.plot(df2['rmse'].values)
plt.ylabel('Frecuecia del valor RMSE')
plt.xlabel('Raíz del error absoluto medio (RMSE)')
plt.title('')
plt.xlim(None, 200)
plt.show()
