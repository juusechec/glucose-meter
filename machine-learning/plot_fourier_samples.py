import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.fftpack
import scipy.signal

def plot_fourier(filename):
    df = pd.read_csv(filename, sep=',', skiprows=4)

    # Number of samplepoints
    N = 10000
    # sample spacing
    T = 0.008
    x = np.linspace(0.0, N * T, N)
    y = df['adc0'].values

    # f = 60
    # sint = 10 * np.sin(2 * np.pi * f * x)
    # y = y + sint

    y = y - np.mean(y)  # remove DC
    yf = scipy.fftpack.fft(y)
    xf = np.linspace(0.0, 1.0 / (2.0 * T), N / 2)
    yfmag = 2.0 / N * np.abs(yf[:N // 2])

    fig, ax = plt.subplots()
    ax.plot(xf, yfmag)
    plt.show()


files = [
    '../microcontroller/samples/output_2019-02-06__21_43_46.txt',
    '../microcontroller/samples/output_2019-02-06__21_54_08.txt',
]

for file in files:
    print(file)
    plot_fourier(file)

# plt.show()