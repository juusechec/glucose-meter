import pywt
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
# import scipy.fftpack

df = pd.read_csv('resources/pruebadedo.csv', sep=',', quotechar='"')

# # Number of samplepoints
# N = 1200
# # sample spacing
# T = 0.02
# x = np.linspace(0.0, N*T, N)
# y = df['Volt'].values
# y = y - np.mean(y) # remove DC
# yf = scipy.fftpack.fft(y)
# xf = np.linspace(0.0, 1.0/(2.0*T), N/2)
#
# fig, ax = plt.subplots()
# ax.plot(xf, 2.0/N * np.abs(yf[:N//2]))
# plt.show()

# cA, cD = pywt.dwt(y, 'db1')
# plt.plot(cD)
# plt.show()

sig = df['Volt'].values
widths = np.arange(1, 31)
cwtmatr, freqs = pywt.cwt(sig, widths, 'mexh')
# plt.plot(cwtmatr)
plt.imshow(cwtmatr, extent=[-1, 1, 1, 31], cmap='PRGn', aspect='auto',
           vmax=abs(cwtmatr).max(), vmin=-abs(cwtmatr).max())
plt.show()