import pywt
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
# import scipy.fftpack

def plot_wavelets(file):
    df = pd.read_csv(file, sep=',', skiprows=4)

    x = df['adc0'].values

    w = pywt.Wavelet('sym5')
    # plt.plot(w.dec_lo)
    coeffs = pywt.wavedec(x, w, level=10)
    print(coeffs[1])
    print(len(coeffs[1]))
    plt.plot(coeffs[1])
    rms = np.sqrt(np.mean(x ** 2))
    print(rms)
    plt.show()

files = [
    '../microcontroller/samples/output_2019-02-12__21_28_23.txt',
    '../microcontroller/samples/output_2019-02-12__21_30_06.txt',
    '../microcontroller/samples/output_2019-02-12__21_31_56.txt',
    '../microcontroller/samples/output_2019-02-12__21_33_58.txt',
    '../microcontroller/samples/output_2019-02-12__21_35_54.txt',
]

for file in files:
    print(file)
    plot_wavelets(file)
