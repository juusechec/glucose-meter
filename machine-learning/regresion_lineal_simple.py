# creditos: http://machinelearningparatodos.com/regresion-lineal-en-python/
import numpy as np
import random
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.pyplot as plt
#% matplotlib inline

# Generador de distribución de datos para regresión lineal simple
def generador_datos_simple(beta, muestras, desviacion):
    # Genero n (muestras) valores de x aleatorios entre 0 y 100
    x = np.random.random(muestras) * 100
    # Genero un error aleatorio gaussiano con desviación típica (desviacion)
    e = np.random.randn(muestras) * desviacion
    # Obtengo el y real como x*beta + error
    y = x * beta + e
    return x.reshape((muestras, 1)), y.reshape((muestras, 1))

# Parámetros de la distribución
desviacion = 200
beta = 10
n = 50
x, y = generador_datos_simple(beta, n, desviacion)

# Represento los datos generados
plt.figure(0)
plt.scatter(x, y)
#plt.show() # descomentar si quiere mostrarse ahí

# Creo un modelo de regresión lineal
modelo = linear_model.LinearRegression()

# Entreno el modelo con los datos (X,Y)
modelo.fit(x, y)
# Ahora puedo obtener el coeficiente b_1
print(u'Coeficiente beta1: ', modelo.coef_[0])

# Podemos predecir usando el modelo
y_pred = modelo.predict(x)

# Por último, calculamos el error cuadrático medio y el estadístico R^2
#error = sum((y - y_pred)**2)/len(y)
#print(error)
plt.figure(1)
plt.plot(y)
plt.plot(y_pred)
print(u'Error cuadrático medio: %.2f' % mean_squared_error(y, y_pred))
print(u'Raiz Error cuadrático medio: %.2f' % np.sqrt(mean_squared_error(y, y_pred)))
print(u'Estadístico R_2: %.2f' % r2_score(y, y_pred))

plt.figure(3)
plt.scatter(x, y)
plt.plot(x, y_pred, color='red')
x_real = np.array([0, 100])
y_real = x_real*beta
plt.plot(x_real, y_real, color='green')
plt.show()