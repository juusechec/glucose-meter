import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from random import randint
from joblib import dump, load

def get_data(filename):
    filename = 'data/' + filename + '.txt'
    temp = pd.read_csv(filename, sep=',', skipfooter=10003, engine='python')
    fingerTemp = temp['fingerTemperatureCelsius(TMP102)'].values[0]
    extTemp = temp['externalTemperatureCelsius(LM35)'].values[0]
    data = pd.read_csv(filename, sep=',', skiprows=4)
    adc0 = data['adc0'].values
    return {
        'fingerTemp': fingerTemp,
        'extTemp': extTemp,
        'rms': (np.sqrt(np.mean((adc0*0.002) ** 2)))/3.4,
        'var': np.var(adc0*0.002),
    }

df = pd.read_csv('data/mediciones.csv', sep=',')
df[ 'TempDedo' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['fingerTemp'], axis = 1 )
df[ 'TempExt' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['extTemp'], axis = 1 )
df[ 'RMSTx' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['rms'], axis = 1 )
df[ 'VarianzaTx' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['var'], axis = 1 )
df[ 'RMSRx' ] = df.apply( lambda row: get_data(row[ 'Reflexión' ])['rms'], axis = 1 )
df[ 'VarianzaRx' ] = df.apply( lambda row: get_data(row[ 'Reflexión' ])['var'], axis = 1 )
df['Edad'] = df.apply( lambda row: row['Edad']/110, axis = 1 )
df['Estatura'] = df.apply( lambda row: row['Estatura']/210, axis = 1 )
df['Peso'] = df.apply( lambda row: row['Peso']/200, axis = 1 )
df['IMC'] = df.apply( lambda row: row['IMC']/50, axis = 1 )
df['Cintura'] = df.apply( lambda row: row['Cintura']/200, axis = 1 )
df['Hijos'] = df.apply( lambda row: row['Hijos']/15, axis = 1 )
df['CantCig'] = df.apply( lambda row: row['CantCig']/560, axis = 1 )
df['Bebidas'] = df.apply( lambda row: row['Bebidas']/105, axis = 1 )
df['Deporte'] = df.apply( lambda row: row['Deporte']/7, axis = 1 )
df['Agua'] = df.apply( lambda row: row['Agua']/196, axis = 1 )
df['Micción'] = df.apply( lambda row: row['Micción']/140, axis = 1 )
df['Glucosa'] = df.apply( lambda row: row['Glucosa']/600, axis = 1 )
df['TempDedo'] = df.apply( lambda row: row['TempDedo']/40, axis = 1 )
df['TempExt'] = df.apply( lambda row: row['TempExt']/50, axis = 1 )
# print(df.corr())

X = df.as_matrix( columns = [
    'Edad',
    'Estatura',
    'Peso',
    'IMC',
    'Cintura',
    'Sexo',
    'Hijos',
    'EnferEmb',
    'Diabetes',
    'CantCig',
    'Bebidas',
    'Deporte',
    'Agua',
    'Sed',
    'Micción',
    'Antecedente',
    'TempDedo',
    'TempExt',
    'RMSTx',
    'VarianzaTx',
    'RMSRx',
    'VarianzaRx'
])
y = df['Glucosa']

X_train, X_test, y_train, y_test = train_test_split(X, y,test_size=0.2,shuffle=True) # test_size=0.1


def create_model(numCapas, neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4, fxActivacion):
    capas = {
        2: (neuronasCapa1, neuronasCapa2),
        3: (neuronasCapa1, neuronasCapa2, neuronasCapa3),
        4: (neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4),
    }

    mlp = MLPRegressor(
        hidden_layer_sizes=capas[numCapas],
        max_iter=5000000,
        activation=fxActivacion,
        shuffle=True,
        solver='lbfgs'
    )

    mlp.fit(X_train, y_train)

    y_pred = mlp.predict(X)

    mse = mean_squared_error(y, y_pred)
    rmse = np.sqrt(mean_squared_error(y, y_pred))
    mae = mean_absolute_error(y, y_pred)
    rmae = np.sqrt(mean_absolute_error(y, y_pred))
    r2 = r2_score(y, y_pred)


    print(u'Error cuadrático medio: {:.10f}'.format(mse))
    print(u'Raiz Error cuadrático medio (RMSE): %.10f' % rmse)
    print(u'Error absoluto medio (MAE): {:.10f}'.format(mae))
    print(u'Raiz Error absoluto medio: %.2f' % rmae)
    print(u'Estadístico R_2: %.10f' % r2)

    params = mlp.get_params()
    print('Params:', params)
    # print('len coef:', len(mlp.coefs_))
    # print('len coef[0]:', len(mlp.coefs_[0]))
    # print('coefs:', mlp.coefs_)
    # print('len intercepts:', len(mlp.intercepts_))
    # print('len intercepts[0]:', len(mlp.intercepts_[0]))
    # print('intercepts:', len(mlp.intercepts_))

    data = {
        'mse': mse,
        'rmse': rmse,
        'mae': mae,
        'rmae': rmae,
        'r2': r2,
        'params': params,
        'coef': mlp.coefs_,
        'intercepts': mlp.intercepts_,
        'y_pred': y_pred
    }
    return rmse, data, mlp

error_deseado = 0.015
error_obtenido = 1
data = None
mlp = None
error_mejor = 1
while error_obtenido >= error_deseado:
    numCapas = randint(2, 4)
    neuronasCapa1 = randint(1, 60)
    neuronasCapa2 = randint(1, 60)
    neuronasCapa3 = randint(1, 60)
    neuronasCapa4 = randint(1, 60)
    indexActivation = randint(0, 3)
    activacion = ('identity', 'logistic', 'tanh', 'relu')
    error_obtenido, data, mlp = create_model(numCapas, neuronasCapa1, neuronasCapa2, neuronasCapa3, neuronasCapa4, activacion[indexActivation])
    if error_obtenido < 0.025:
        error_menor = error_obtenido
        if error_menor <= error_mejor:
            print('Datos:', data)
            print('error_obtenido', error_obtenido)
            dump(mlp, 'resources/MLPRegressormenores.joblib')
            error_mejor=error_menor
            print('Encontre uno',error_mejor)

dump(mlp, 'resources/MLPRegressorbest2.joblib')
print('Datos finales:', data)
print('error_obtenido final', error_obtenido)
plt.plot(y, color='r')
plt.plot(data['y_pred'], color='b')
plt.show()
