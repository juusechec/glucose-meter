#!/usr/bin/env python3
import pandas as pd
import numpy as np

def get_data(filename):
    filename = 'data/' + filename + '.txt'
    temp = pd.read_csv(filename, sep=',', skipfooter=10003, engine='python')
    fingerTemp = temp['fingerTemperatureCelsius(TMP102)'].values[0]
    extTemp = temp['externalTemperatureCelsius(LM35)'].values[0]
    data = pd.read_csv(filename, sep=',', skiprows=4)
    adc0 = data['adc0'].values
    rms = np.sqrt(np.mean(adc0 ** 2))
    return {
        "fingerTemp": fingerTemp,
        "extTemp": extTemp,
        "rms": rms
    }

df = pd.read_csv('data/mediciones.csv', sep=',')
df[ 'TempDedo' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['fingerTemp'], axis = 1 )
df[ 'TempExt' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['extTemp'], axis = 1 )
df[ 'RMS' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['rms'], axis = 1 )
df.describe()

X = df.as_matrix( columns = [
    'Edad',
    'Estatura',
    'Peso',
    'IMC',
    'Cintura',
    'Sexo',
    'Hijos',
    'EnferEmb',
    'Diabetes',
    'CantCig',
    'Bebidas',
    'Deporte',
    'Agua',
    'Sed',
    'Micción',
    'Antecedente',
    'TempDedo',
    'TempExt',
    'RMS'
])
