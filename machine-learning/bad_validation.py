import numpy as np
import pandas as pd
# from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error
from sklearn.model_selection import train_test_split
# from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler

from constants import *

# mlp = MLPRegressor(
#         hidden_layer_sizes=(24, 18, 14, 12),
#         max_iter=5000000,
#         activation='tanh',
#         solver='lbfgs',
#         warm_start=True
#     )
#
# mlp.coefs_ = coefs
# mlp.intercepts_ = intercepts
# mlp.set_params(**parameters)
# mlp.n_outputs_ = 1
# mlp.n_layers_ = 4
# mlp.out_activation_ = 'tanh'
# layer_units = ([len(columns)] + list(mlp.hidden_layer_sizes) + [mlp.n_outputs_])
# mlp.n_layers_ = len(layer_units)

def get_data(filename):
    filename = 'data/' + filename + '.txt'
    temp = pd.read_csv(filename, sep=',', skipfooter=10003, engine='python')
    fingerTemp = temp['fingerTemperatureCelsius(TMP102)'].values[0]
    extTemp = temp['externalTemperatureCelsius(LM35)'].values[0]
    data = pd.read_csv(filename, sep=',', skiprows=4)
    adc0 = data['adc0'].values
    return {
        'fingerTemp': fingerTemp,
        'extTemp': extTemp,
        'rms': np.sqrt(np.mean(adc0 ** 2)),
        'var': np.var(adc0)
    }

df = pd.read_csv('data/mediciones.csv', sep=',')
df[ 'TempDedo' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['fingerTemp'], axis = 1 )
df[ 'TempExt' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['extTemp'], axis = 1 )
df[ 'RMSTx' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['rms'], axis = 1 )
df[ 'VarianzaTx' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['var'], axis = 1 )
df[ 'RMSRx' ] = df.apply( lambda row: get_data(row[ 'Reflexión' ])['rms'], axis = 1 )
df[ 'VarianzaRx' ] = df.apply( lambda row: get_data(row[ 'Reflexión' ])['var'], axis = 1 )

# print(df.corr())

X = df.as_matrix( columns = columns)
y = df['Glucosa']

# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.01) # test_size=0.1
#
#
# scaler = StandardScaler()
# # Fit only to the training data
# scaler.fit(X_train)
# # Now apply the transformations to the data:
# X_train = scaler.transform(X_train)
# X_test = scaler.transform(X_test)


# verboseMode = False
verboseMode = True
def verbose(*args, **kwargs):
    global verboseMode
    if verboseMode:
        print(*args, **kwargs)

coefs_ = pd.DataFrame(coefs)

verbose('Numero de entradas: {}'.format(len(columns)))

for i in range(len(coefs_)):
    number_neurons_in_layer = len(coefs_.iloc[i][1])
    verbose('Para la capa {} hay {} neuronas'.format(i+1, number_neurons_in_layer))
    for j in range(number_neurons_in_layer):
        weights = coefs_.iloc[i][j]
        verbose(i, j, weights, end=", ")
        verbose()
    verbose()

verbose("Valores de Bias para la capa de entrada:")
verbose(intercepts[0])
verbose("Valores de Bias para la primera capa oculta:")
verbose(intercepts[1])
verbose("Valores de Bias para la segunda capa oculta:")
verbose(intercepts[2])
verbose("Valores de Bias para la tercera capa oculta:")
verbose(intercepts[3])
verbose("Valores de Bias para la capa de salida:")
verbose(intercepts[4])

def tanh(x):
    return np.tanh(x)

layer_size = (24, 18, 14, 12, 1)

nombreCapaN = {
    0: 'X',
    1: 'A',
    2: 'B',
    3: 'C',
    4: 'D',
    5: 'Y'
}

neuCapaN = [None] * len(layer_size)

numMuestra = 22

def entradaN():
    return {
        0: X[numMuestra], # cambiar por un for al vector entero
        1: neuCapaN[0],
        2: neuCapaN[1],
        3: neuCapaN[2],
        4: neuCapaN[3]
    }

for n, capaN in enumerate(coefs):
    verbose('Capa {}'.format(n))
    neuCapaN[n] = [0] * layer_size[n]
    for i, coefWiVector in enumerate(capaN):
        # verbose(i, coefXiVector)
        for j, coefWijConst in enumerate(coefWiVector):
            # verbose('Entrada X_i {} y neurona A_j {}'.format(i, j), coefAjConst)
            verbose('neu{}_{} += coefW_{}_{} * ent{}_{} ({},{},{})'.format(nombreCapaN[n+1], j, i, j, nombreCapaN[n],i, n, i, j))
            neuCapaN[n][j] += coefWijConst * entradaN()[n][i]
    for k, neu in enumerate(neuCapaN[n]):
        verbose('SumNeu{}'.format(k), neu)
        neuCapaN[n][k] += intercepts[n][k]
        verbose('InterceptBias{}_{}'.format(n, k), neuCapaN[n][k])
        neuCapaN[n][k] = tanh(neuCapaN[n][k])
        # verbose('FuncActi{}'.format(k), neuCapaN[n][k])
    verbose('neuCapa{}'.format(n), neuCapaN[n])

print('Esperado {} y regresión calculado {}'.format(y[numMuestra], neuCapaN[-1][0]))
# mse = mean_squared_error(y_test, y_pred)
# rmse = np.sqrt(mse)
# mae = mean_absolute_error(y_test, y_pred)
# rmae = np.sqrt(mae)
# r2 = r2_score(y_test, y_pred)
#
# print(u'Error cuadrático medio: {:.10f}'.format(mse))
# print(u'Raiz Error cuadrático medio (RMSE): %.10f' % rmse)
# print(u'Error absoluto medio (MAE): {:.10f}'.format(mae))
# print(u'Raiz Error absoluto medio: %.2f' % rmae)
# print(u'Estadístico R_2: %.10f' % r2)


