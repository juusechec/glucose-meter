# import numpy as np
# import pandas as pd
# import matplotlib.pyplot as plt
# from sklearn.linear_model import LinearRegression
# from sklearn.svm import SVR
# from sklearn.model_selection import train_test_split
#
# x = np.linspace(0,100,101)
# y = np.array([(100*np.random.rand(1)+num) for num in (5*x+10)])
#
# x_train, x_test, y_train, y_test = train_test_split(x, y)
#
# svr = SVR(kernel='linear')
# lm = LinearRegression()
# svr.fit(x_train.reshape(-1,1),y_train.flatten())
# lm.fit(x_train.reshape(-1,1), y_train.flatten())
#
# pred_SVR = svr.predict(x_test.reshape(-1,1))
# pred_lm = lm.predict(x_test.reshape(-1,1))
#
# plt.plot(x,y, label='True data')
# plt.plot(x_test[::2], pred_SVR[::2], 'co', label='SVR')
# plt.plot(x_test[1::2], pred_lm[1::2], 'mo', label='Linear Reg')
# plt.legend(loc='upper left');
# plt.show();

from sklearn import svm
from sklearn import datasets

clf = svm.SVC(gamma=0.001, C=100.)
iris = datasets.load_iris()
X, y = iris.data, iris.target
clf.fit(X, y)


