import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from joblib import dump, load
from constants import *

def get_data(filename):
    filename = 'data/' + filename + '.txt'
    temp = pd.read_csv(filename, sep=',', skipfooter=10003, engine='python')
    fingerTemp = temp['fingerTemperatureCelsius(TMP102)'].values[0]
    extTemp = temp['externalTemperatureCelsius(LM35)'].values[0]
    data = pd.read_csv(filename, sep=',', skiprows=4)
    adc0 = data['adc0'].values
    return {
        'fingerTemp': fingerTemp,
        'extTemp': extTemp,
        'rms': np.sqrt(np.mean(adc0 ** 2)),
        'var': np.var(adc0)
    }

df = pd.read_csv('data/mediciones.csv', sep=',')
df[ 'TempDedo' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['fingerTemp'], axis = 1 )
df[ 'TempExt' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['extTemp'], axis = 1 )
df[ 'RMSTx' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['rms'], axis = 1 )
df[ 'VarianzaTx' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['var'], axis = 1 )
df[ 'RMSRx' ] = df.apply( lambda row: get_data(row[ 'Reflexión' ])['rms'], axis = 1 )
df[ 'VarianzaRx' ] = df.apply( lambda row: get_data(row[ 'Reflexión' ])['var'], axis = 1 )

# print(df.corr())

X = df.as_matrix( columns = columns)
y = df['Glucosa']

# X_train, X_test, y_train, y_test = train_test_split(X, y) # test_size=0.1

# delete, only for testing
X_train = X
y_train = y
X_test = X_train
y_test = y_train

scaler = StandardScaler()
scaler.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

mlp = load('resources/MLPRegressor.joblib')

y_pred = mlp.predict(X_test)

mse = mean_squared_error(y_test, y_pred)
rmse = np.sqrt(mean_squared_error(y_test, y_pred))
mae = mean_absolute_error(y_test, y_pred)
rmae = np.sqrt(mean_absolute_error(y_test, y_pred))
r2 = r2_score(y_test, y_pred)

print(u'Error cuadrático medio: {:.10f}'.format(mse))
print(u'Raiz Error cuadrático medio (RMSE): %.10f' % rmse)
print(u'Error absoluto medio (MAE): {:.10f}'.format(mae))
print(u'Raiz Error absoluto medio: %.2f' % rmae)
print(u'Estadístico R_2: %.10f' % r2)



sorted_test = y_test.sort_values()
sorted_pred = pd.Series(y_pred)[sorted_test.index]
plt.plot(sorted_test.values, color='b')
plt.plot(sorted_pred.values, color='r')
plt.show()