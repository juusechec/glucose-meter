# creditos: http://machinelearningparatodos.com/regresion-lineal-en-python/
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.model_selection import train_test_split


def get_data(filename):
    filename = 'data/' + filename + '.txt'
    temp = pd.read_csv(filename, sep=',', skipfooter=10003, engine='python')
    fingerTemp = temp['fingerTemperatureCelsius(TMP102)'].values[0]
    extTemp = temp['externalTemperatureCelsius(LM35)'].values[0]
    data = pd.read_csv(filename, sep=',', skiprows=4)
    adc0 = data['adc0'].values
    rms = np.sqrt(np.mean(adc0 ** 2))
    return {
        "fingerTemp": fingerTemp,
        "extTemp": extTemp,
        "rms": rms
    }

df = pd.read_csv('data/mediciones.csv', sep=',')
df[ 'TempDedo' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['fingerTemp'], axis = 1 )
df[ 'TempExt' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['extTemp'], axis = 1 )
df[ 'RMS' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['rms'], axis = 1 )

# X = df.as_matrix( columns = ['Edad','Estatura','Peso','IMC','Cintura','Sexo','Hijos','EnferEmb','Fuma','CantCig','Bebidas','Deporte','Agua','Sed','Micción','Antecedente','c0','c1','c2','c3'] )
X = df.as_matrix( columns = [
    'Edad',
    'Estatura',
    'Peso',
    'IMC',
    'Cintura',
    'Sexo',
    'Hijos',
    'EnferEmb',
    'Diabetes',
    'CantCig',
    'Bebidas',
    'Deporte',
    'Agua',
    'Sed',
    'Micción',
    'Antecedente',
    'TempDedo',
    'TempExt',
    'RMS'
])
Y = df['Glucosa']

# Creo un modelo de regresión lineal
modelo = linear_model.LinearRegression()

X_train, X_test, Y_train, Y_test = train_test_split(X, Y)

# Entreno el modelo con los datos (X,Y)
modelo.fit(X_train, Y_train)
# Ahora puedo obtener el coeficiente b_1
# https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html#sklearn.linear_model.LinearRegression.fit
print (u'Num Coeficientes: ', len(modelo.coef_))
print (u'Coeficiente beta1: ', modelo.coef_[0])
print (u'Coeficientes: ', modelo.coef_)

# Podemos predecir usando el modelo
y_pred = modelo.predict(X_test)

# Por último, calculamos el error cuadrático medio y el estadístico R^2
# https://pyformat.info/
# http://ligdigonzalez.com/evaluando-el-error-en-los-modelos-de-regresion/
print(u'Error cuadrático medio: {:.4f}'.format(mean_squared_error(Y_test, y_pred)))
print(u'Raiz Error cuadrático medio (RMSE): %.2f' % np.sqrt(mean_squared_error(Y_test, y_pred)))
print(u'Error absoluto medio (MAE): {:.4f}'.format(mean_absolute_error(Y_test, y_pred)))
print(u'Raiz Error absoluto medio: %.2f' % np.sqrt(mean_absolute_error(Y_test, y_pred)))
print(u'Estadístico R_2: %.2f' % r2_score(Y_test, y_pred))

Y.plot()

def calculate_glucose(row, coef, intercept):
    return intercept\
           + (coef[0] * row['Edad'])\
           + (coef[1] * row['Estatura'])\
           + (coef[2] * row['Peso'])\
           + (coef[3] * row['IMC'])\
           + (coef[4] * row['Cintura'])\
           + (coef[5] * row['Sexo'])\
           + (coef[6] * row['Hijos'])\
           + (coef[7] * row['EnferEmb'])\
           + (coef[8] * row['Diabetes'])\
           + (coef[9] * row['CantCig'])\
           + (coef[10] * row['Bebidas'])\
           + (coef[11] * row['Deporte'])\
           + (coef[12] * row['Agua'])\
           + (coef[13] * row['Sed'])\
           + (coef[14] * row['Micción'])\
           + (coef[15] * row['Antecedente'])\
           + (coef[16] * row['TempDedo'])\
           + (coef[17] * row['TempExt'])\
           + (coef[18] * row['RMS'])

df['GlucosaPredecida'] = df.apply(lambda row: calculate_glucose(row, modelo.coef_, modelo.intercept_), axis=1)


df['Glucosa'].plot(color='r')
df['GlucosaPredecida'].plot(color='b')
plt.legend(('Glucosa', 'Regresión Glucosa'),
           loc='upper right')
plt.title('Glucosa medida vs Glucosa estimado')
plt.xlabel('Muestras')
plt.ylabel('Concentración (ml/g)')
# plt.plot(y_pred, color='g')

plt.show()