import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import scipy.fftpack
import scipy.signal

df = pd.read_csv('resources/pruebadedo.csv', sep=',', quotechar='"')

# Number of samplepoints
N = 1200
# sample spacing
T = 0.02
x = np.linspace(0.0, N*T, N)
y = df['Volt'].values


y = y - np.mean(y) # remove DC
yf = scipy.fftpack.fft(y)
xf = np.linspace(0.0, 1.0/(2.0*T), N/2)
yfmag = 2.0/N * np.abs(yf[:N//2]);

# fig, ax = plt.subplots()
# ax.plot(xf, yfmag)
# plt.show()

# https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html
z = scipy.signal.find_peaks(yfmag)
def nullify(val, index):
    if index in z[0]:
        return val
    else:
        return 0

nullifycated = np.zeros(len(yfmag))

for index, val in enumerate(yfmag.tolist()):
    nullifycated[index] = nullify(val, index)
# plt.plot(nullifycated)
# plt.show()

indexofmax = [i[0] for i in sorted(enumerate(nullifycated), reverse=True, key=lambda x:x[1])]
indexofmax = indexofmax[0:10]

def nullify2(val, index):
    if index in indexofmax:
        return val
    else:
        return 0
ymaximun = np.zeros(len(yfmag))
for index, val in enumerate(yfmag.tolist()):
    ymaximun[index] = nullify2(val, index)

# Now switch to a more OO interface to exercise more features.
fig, axs = plt.subplots(nrows=1, ncols=2, sharex=True)
ax = axs[0]
ax.plot(xf, yfmag)
ax.set_title('dfft')

ax = axs[1]
# ax.plot(xf, nullifycated)
ax.scatter(xf, ymaximun)
ax.set_title('peaks')


fig.suptitle('peaks vs dfft')

plt.show()
