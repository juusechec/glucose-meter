# creditos: http://www.xavierdupre.fr/app/mlinsights/helpsphinx/notebooks/quantile_mlpregression.html
import numpy
X = numpy.random.random(1000)
eps1 = (numpy.random.random(900) - 0.5) * 0.1
eps2 = (numpy.random.random(100)) * 10
eps = numpy.hstack([eps1, eps2])
X = X.reshape((1000, 1))
Y = X.ravel() * 3.4 + 5.6 + eps

from sklearn.neural_network import MLPRegressor
clr = MLPRegressor(hidden_layer_sizes=(30,), activation='tanh')
clr.fit(X, Y)


from mlinsights.mlmodel import QuantileMLPRegressor
clq = QuantileMLPRegressor(hidden_layer_sizes=(30,), activation='tanh')
clq.fit(X, Y)

from pandas import DataFrame
data= dict(X=X.ravel(), Y=Y, clr=clr.predict(X), clq=clq.predict(X))
df = DataFrame(data)
df.head()

import matplotlib.pyplot as plt
fig, ax = plt.subplots(1, 1, figsize=(10, 4))
choice = numpy.random.choice(X.shape[0]-1, size=100)
xx = X.ravel()[choice]
yy = Y[choice]
ax.plot(xx, yy, '.', label="data")
xx = numpy.array([[0], [1]])
y1 = clr.predict(xx)
y2 = clq.predict(xx)
ax.plot(xx, y1, "--", label="L2")
ax.plot(xx, y2, "--", label="L1")
ax.set_title("Quantile (L1) vs Square (L2) for MLPRegressor")
ax.legend()