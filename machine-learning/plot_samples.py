#!/usr/bin/env python3
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

def plot(file):
    df = pd.read_csv(file, sep=',', skiprows=4)

    # Number of samplepoints
    # N = 100 # ADC
    N = 10000 #
    # N = 928 # osciloscopio
    # sample spacing
    T = 0.008 # ADC
    # T = 0.1 # osciloscopio
    t = np.linspace(0.0, N*T, N)

    y = df['adc0'].values
    y = y * 0.002
    # y = df['adc0'].values[1170:1270]
    # y = df.values # osciloscopio
    # y = y - np.mean(y) # remove DC
    rms = np.sqrt(np.mean(y**2))
    print(rms)
    # print(max(y))
    fig = plt.figure(0)
    fig.canvas.set_window_title(file)
    plt.plot(t, y)
    plt.xlabel('Tiempo / s')
    plt.ylabel('Voltaje / v')
    plt.xlim(0, None)
    plt.ylim(0, None)
    plt.title('')
    # plt.ylim(0, None) # starts from zero
    plt.show()

files = [
    'data/output_2019-02-09__17_24_32.txt',
    'data/output_2019-02-09__17_26_30.txt',
    'data/output_2019-02-09__17_37_28.txt',
    'data/output_2019-02-09__17_39_11.txt',
    'data/output_2019-02-09__17_45_22.txt',
    'data/output_2019-02-09__17_47_56.txt',
    'data/output_2019-02-09__17_56_04.txt',
    'data/output_2019-02-09__17_57_45.txt',
    'data/output_2019-02-09__18_02_57.txt',
    'data/output_2019-02-09__18_05_12.txt',
    'data/output_2019-02-09__18_17_27.txt',
    'data/output_2019-02-09__18_19_11.txt',
    'data/output_2019-02-09__18_32_03.txt',
    'data/output_2019-02-09__18_33_47.txt',
    'data/output_2019-02-09__18_52_58.txt',
    'data/output_2019-02-09__18_54_40.txt',
    'data/output_2019-02-09__18_59_32.txt',
    'data/output_2019-02-09__19_01_24.txt',
    'data/output_2019-02-09__19_10_46.txt',
    'data/output_2019-02-09__19_12_31.txt',
    'data/output_2019-02-09__19_14_28.txt',
    'data/output_2019-02-09__19_16_10.txt',
    'data/output_2019-02-09__19_18_15.txt',
    'data/output_2019-02-09__19_19_58.txt',
    'data/output_2019-02-09__19_27_31.txt',
    'data/output_2019-02-09__19_29_30.txt',
    'data/output_2019-02-09__19_32_42.txt',
    'data/output_2019-02-09__19_34_41.txt',
    'data/output_2019-02-09__19_37_39.txt',
    'data/output_2019-02-09__19_39_41.txt',
    'data/output_2019-02-09__20_01_37.txt',
    'data/output_2019-02-09__20_03_22.txt',
    'data/output_2019-02-09__20_13_44.txt',
    'data/output_2019-02-09__20_15_28.txt',
    'data/output_2019-02-09__20_28_36.txt',
    'data/output_2019-02-09__20_30_21.txt',
    'data/output_2019-02-09__20_35_02.txt',
    'data/output_2019-02-09__20_36_46.txt',
    'data/output_2019-02-09__20_44_09.txt',
    'data/output_2019-02-09__20_46_13.txt',
    'data/output_2019-02-09__21_06_02.txt',
    'data/output_2019-02-09__21_07_59.txt',
    'data/output_2019-02-10__10_25_45.txt',
    'data/output_2019-02-10__10_27_30.txt',
    'data/output_2019-02-10__10_36_01.txt',
    'data/output_2019-02-10__10_38_32.txt',
    'data/output_2019-02-10__10_40_35.txt',
    'data/output_2019-02-10__10_42_34.txt',
    'data/output_2019-02-10__11_16_07.txt',
    'data/output_2019-02-10__11_17_57.txt',
    'data/output_2019-02-10__11_28_25.txt',
    'data/output_2019-02-10__11_30_08.txt',
    'data/output_2019-02-10__11_37_11.txt',
    'data/output_2019-02-10__11_38_54.txt',
    'data/output_2019-02-10__11_46_58.txt',
    'data/output_2019-02-10__11_48_47.txt',
    'data/output_2019-02-10__11_56_25.txt',
    'data/output_2019-02-10__11_58_31.txt',
    'data/output_2019-02-10__12_02_58.txt',
    'data/output_2019-02-10__12_04_45.txt',
    'data/output_2019-02-10__12_10_28.txt',
    'data/output_2019-02-10__12_12_39.txt',
    'data/output_2019-02-10__12_25_02.txt',
    'data/output_2019-02-10__12_26_52.txt',
    'data/output_2019-02-10__12_34_40.txt',
    'data/output_2019-02-10__12_36_20.txt',
    'data/output_2019-02-10__12_43_28.txt',
    'data/output_2019-02-10__12_45_11.txt',
    'data/output_2019-02-10__12_52_05.txt',
    'data/output_2019-02-10__12_54_06.txt',
    'data/output_2019-02-10__13_01_16.txt',
    'data/output_2019-02-10__13_03_07.txt',
    'data/output_2019-02-10__13_08_44.txt',
    'data/output_2019-02-10__13_10_38.txt',
    'data/output_2019-02-10__15_45_32.txt',
    'data/output_2019-02-10__15_47_30.txt',
    'data/output_2019-02-10__15_51_18.txt',
    'data/output_2019-02-10__15_52_59.txt',
    'data/output_2019-02-10__15_57_19.txt',
    'data/output_2019-02-10__15_59_00.txt',
    'data/output_2019-02-10__16_22_01.txt',
    'data/output_2019-02-10__16_23_46.txt',
    'data/output_2019-02-10__16_31_21.txt',
    'data/output_2019-02-10__16_33_04.txt',
    'data/output_2019-02-10__16_41_25.txt',
    'data/output_2019-02-10__16_43_07.txt',
    'data/output_2019-02-10__16_52_30.txt',
    'data/output_2019-02-10__16_55_29.txt',
    'data/output_2019-02-10__17_03_12.txt',
    'data/output_2019-02-10__17_04_54.txt',
    'data/output_2019-02-10__17_34_55.txt',
    'data/output_2019-02-10__17_39_04.txt',
    'data/output_2019-02-10__17_47_02.txt',
    'data/output_2019-02-10__17_49_00.txt',
    'data/output_2019-02-10__17_53_22.txt',
    'data/output_2019-02-10__17_55_11.txt',
    'data/output_2019-02-11__06_22_39.txt',
    'data/output_2019-02-11__06_24_41.txt',
    'data/output_2019-02-11__06_35_04.txt',
    'data/output_2019-02-11__06_36_46.txt',
    'data/output_2019-02-11__06_40_36.txt',
    'data/output_2019-02-11__06_42_21.txt',
    'data/output_2019-02-11__06_46_48.txt',
    'data/output_2019-02-11__06_49_29.txt',
    'data/output_2019-02-11__06_57_16.txt',
    'data/output_2019-02-11__06_59_11.txt',
    'data/output_2019-02-11__07_05_56.txt',
    'data/output_2019-02-11__07_07_43.txt',
    'data/output_2019-02-11__07_13_08.txt',
    'data/output_2019-02-11__07_15_37.txt',
    'data/output_2019-02-11__07_27_35.txt',
    'data/output_2019-02-11__07_29_59.txt',
    'data/output_2019-02-11__07_38_31.txt',
    'data/output_2019-02-11__07_40_13.txt',
    'data/output_2019-02-11__08_04_04.txt',
    'data/output_2019-02-11__08_05_44.txt',
    'data/output_2019-02-11__08_24_20.txt',
    'data/output_2019-02-11__08_26_12.txt',
]

for file in files:
    print(file)
    plot(file)

# plt.show()