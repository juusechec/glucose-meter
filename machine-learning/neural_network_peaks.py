import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report, confusion_matrix

def get_data(filename):
    filename = 'data/' + filename + '.txt'
    temp = pd.read_csv(filename, sep=',', skipfooter=10003, engine='python')
    fingerTemp = temp['fingerTemperatureCelsius(TMP102)'].values[0]
    extTemp = temp['externalTemperatureCelsius(LM35)'].values[0]
    data = pd.read_csv(filename, sep=',', skiprows=4)
    adc0 = data['adc0'].values
    return {
        'fingerTemp': fingerTemp,
        'extTemp': extTemp,
        'rms': np.sqrt(np.mean(adc0 ** 2)),
        'var': np.var(adc0)
    }

df = pd.read_csv('data/mediciones.csv', sep=',')
df[ 'TempDedo' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['fingerTemp'], axis = 1 )
df[ 'TempExt' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['extTemp'], axis = 1 )
df[ 'RMS' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['rms'], axis = 1 )
df[ 'Varianza' ] = df.apply( lambda row: get_data(row[ 'Transmisión' ])['var'], axis = 1 )

X = df.as_matrix( columns = [
    'Edad',
    'Estatura',
    'Peso',
    'IMC',
    'Cintura',
    'Sexo',
    'Hijos',
    'EnferEmb',
    'Diabetes',
    'CantCig',
    'Bebidas',
    'Deporte',
    'Agua',
    'Sed',
    'Micción',
    'Antecedente',
    'TempDedo',
    'TempExt',
    'RMS',
    'Varianza'
])
y = df['Glucosa']

X_train, X_test, y_train, y_test = train_test_split(X, y)

# delete, only for testing
# X_train = X
# y_train = y
# X_test = X_train
# y_test = y_train

scaler = StandardScaler()
# Fit only to the training data
scaler.fit(X_train)
# Now apply the transformations to the data:
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

mlp = MLPClassifier(hidden_layer_sizes=(20, 5), max_iter=5000000000)

mlp.fit(X_train, y_train)

predictions = mlp.predict(X_test)

print(confusion_matrix(y_test, predictions))

print(classification_report(y_test, predictions))

len(mlp.coefs_)
len(mlp.coefs_[0])
len(mlp.intercepts_[0])

plt.plot(y_test.values, color='r')
plt.plot(predictions, color='b')
plt.show()
