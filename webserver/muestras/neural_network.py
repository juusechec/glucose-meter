import numpy as np
import pandas as pd
from joblib import load

def get_rms(signal):
    return (np.sqrt(np.mean((signal[0]*0.002) ** 2)))/3.4

def get_variance(signal):
    return np.var(signal * 0.002)

def neural_network(cuestionario, medida):
    # print('Medida:', medida)
    # print('Cuestionario:', cuestionario)
    signal_transmision = medida['senal_transmision']
    signal_transmision = pd.DataFrame(signal_transmision).values
    rms_tx = get_rms(signal_transmision)
    variance_tx = get_variance(signal_transmision)

    signal_reflexion = medida['senal_reflexion']
    signal_reflexion = pd.DataFrame(signal_reflexion).values
    rms_rx = get_rms(signal_reflexion)
    variance_rx = get_variance(signal_reflexion)

    columns = [
        'Edad',
        'Estatura',
        'Peso',
        'IMC',
        'Cintura',
        'Sexo',
        'Hijos',
        'EnferEmb',
        'Diabetes',
        'CantCig',
        'Bebidas',
        'Deporte',
        'Agua',
        'Sed',
        'Micción',
        'Antecedente',
        'TempDedo',
        'TempExt',
        'RMSTx',
        'VarianzaTx',
        'RMSRx',
        'VarianzaRx',
    ]

    data = [
        cuestionario.edad/110,
        cuestionario.estatura/210,
        cuestionario.peso/200,
        cuestionario.imc/50,
        cuestionario.cintura/200,
        cuestionario.sexo,
        cuestionario.numero_hijos/15,
        int(cuestionario.diabetes_gestacional),
        int(cuestionario.diabetes),
        cuestionario.cigarrillos_semana/560,
        cuestionario.bebida_azucarada_semana/105,
        cuestionario.deporte_semana/7,
        cuestionario.vasos_agua_semana/196,
        int(cuestionario.sed),
        cuestionario.miccion_semana/140,
        int(cuestionario.antecedentes_familiares_glucosa),
        medida['temperatura_dedo']/40,
        medida['temperatura_externa']/50,
        rms_tx,
        variance_tx,
        rms_rx,
        variance_rx,
    ]
    # print('data', data)
    X = pd.DataFrame([data], columns=columns)
    print('dataframe', X.transpose())
    mlp = load('./muestras/resources/MLPRegressor_glucose.joblib')
    y_pred = mlp.predict(X)
    # print('y_pred', y_pred)
    glucosa = y_pred[0] * 600
    print('glucosa', glucosa)
    return glucosa