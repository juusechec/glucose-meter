from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from muestras.models import Cuestionario, Medida
from muestras.neural_network import neural_network
from muestras.serializers import CuestionarioSerializer, MedidaSerializer
from django.shortcuts import render
# Create your views here.

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

def response_options():
    response = HttpResponse(status=200)
    response['Allow'] = 'OPTIONS, GET, POST'
    response['Access-Control-Request-Method'] = 'OPTIONS, GET, POST'
    response['Access-Control-Request-Headers'] = 'Content-Type'
    response['Access-Control-Allow-Headers'] = 'Content-Type'
    return response

def response_cors(response):
    response['Access-Control-Allow-Origin'] = '*'
    return response

@csrf_exempt
def cuestionario_list(request):
    """
    List all code serie, or create a new serie.
    """
    if request.method == 'GET':
        cuestionario = Cuestionario.objects.all()
        serializer = CuestionarioSerializer(cuestionario, many=True)
        return response_cors(JSONResponse(serializer.data))

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        print('request cuestionario:', data)
        serializer = CuestionarioSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return response_cors(JSONResponse(serializer.data, status=201))
        return response_cors(JSONResponse(serializer.errors, status=400))
    elif request.method == 'OPTIONS':
        return response_cors(response_options())

@csrf_exempt
def cuestionario_detail(request, pk):
    """
    Retrieve, update or delete a serie.
    """
    try:
        cuestionario = Cuestionario.objects.get(pk=pk)
    except Cuestionario.DoesNotExist:
        return response_cors(HttpResponse(status=404))

    if request.method == 'GET':
        serializer = CuestionarioSerializer(cuestionario)
        return response_cors(JSONResponse(serializer.data))

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = CuestionarioSerializer(cuestionario, data=data)
        if serializer.is_valid():
            serializer.save()
            return response_cors(JSONResponse(serializer.data))
        return response_cors(JSONResponse(serializer.errors, status=400))

    elif request.method == 'DELETE':
        cuestionario.delete()
        return response_cors(HttpResponse(status=204))

    elif request.method == 'OPTIONS':
        return response_cors(response_options())


@csrf_exempt
def medida_list(request):
    """
    List all code serie, or create a new serie.
    """
    if request.method == 'GET':
        medida = Medida.objects.all()
        serializer = MedidaSerializer(medida, many=True)
        return response_cors(JSONResponse(serializer.data))

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = MedidaSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return response_cors(JSONResponse(serializer.data, status=201))
        return response_cors(JSONResponse(serializer.errors, status=400))

    elif request.method == 'OPTIONS':
        return response_cors(response_options())

@csrf_exempt
def medida_detail(request, pk):
    """
    Retrieve, update or delete a serie.
    """
    try:
        medida = Medida.objects.get(pk=pk)
    except Medida.DoesNotExist:
        return response_cors(HttpResponse(status=404))

    if request.method == 'GET':
        serializer = MedidaSerializer(medida)
        return response_cors(JSONResponse(serializer.data))

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = MedidaSerializer(medida, data=data)
        if serializer.is_valid():
            serializer.save()
            return response_cors(JSONResponse(serializer.data))
        return response_cors(JSONResponse(serializer.errors, status=400))

    elif request.method == 'DELETE':
        medida.delete()
        return response_cors(HttpResponse(status=204))

    elif request.method == 'OPTIONS':
        return response_cors(response_options())

@csrf_exempt
def datos_compuesto_list(request):
    """
    """
    if request.method == 'GET':
        return response_cors(HttpResponse(status=501))

    elif request.method == 'POST':
        data_medida = JSONParser().parse(request)
        # print(data_medida)
        # serializer = MuestraSerializer(data=data)
        try:
            cuestionario = Cuestionario.objects.get(pk=data_medida['id_cuestionario'])
        except Cuestionario.DoesNotExist:
            return response_cors(HttpResponse(status=404))

        medida = {
            'fecha_hora': data_medida['fecha_hora'],
            'temperatura_dedo': data_medida['temperatura_dedo'],
            'temperatura_externa': data_medida['temperatura_externa'],
            'senal_transmision': ','.join(str(s) for s in data_medida['senal_transmision']),
            'senal_reflexion': ','.join(str(s) for s in data_medida['senal_reflexion']),
            'cuestionario': cuestionario.pk
        }

        serializer = MedidaSerializer(data=medida)
        if serializer.is_valid():
            serializer.save()
            # return response_cors(JSONResponse(serializer.data))
            serializer_medida = serializer.data
            glucosa = neural_network(cuestionario, data_medida)
            # serializer_medida.pop('senal_transmision')
            # serializer_medida.pop('senal_reflexion')
            response = {
                'glucosa': glucosa
            }
            return response_cors(JSONResponse(response, status=200))
        else:
            return response_cors(JSONResponse(serializer.errors, status=400))

    elif request.method == 'OPTIONS':
        return response_cors(response_options())