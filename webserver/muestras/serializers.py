from rest_framework import serializers
from .models import Cuestionario, Medida

class CuestionarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cuestionario
        fields = (
            'id',
            'fecha_hora',
            'edad',
            'estatura',
            'peso',
            'imc',
            'cintura',
            'sexo',
            'numero_hijos',
            'diabetes_gestacional',
            'diabetes',
            'cigarrillos_semana',
            'bebida_azucarada_semana',
            'deporte_semana',
            'vasos_agua_semana',
            'sed',
            'miccion_semana',
            'antecedentes_familiares_glucosa'
        )

class MedidaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medida
        fields = (
            'id',
            'fecha_hora',
            'temperatura_dedo',
            'temperatura_externa',
            'senal_transmision',
            'senal_reflexion',
            'cuestionario'
        )




