from django.conf.urls import url
from muestras import views


urlpatterns = [
    url(r'^cuestionarios/$', views.cuestionario_list),
    url(r'^cuestionarios/(?P<pk>[0-9]+)/$', views.cuestionario_detail),
    url(r'^medidas/$', views.medida_list),
    url(r'^medidas/(?P<pk>[0-9]+)/$', views.medida_detail),
    url(r'^datos_compuestos/$', views.datos_compuesto_list),
]
