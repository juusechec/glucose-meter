from django.db import models

# Create your models here.

class Cuestionario(models.Model):
    SEX_CHOICES = (
        (0, 'Masculino'),
        (1, 'Femenino'),
    )
    fecha_hora = models.DateTimeField()
    edad = models.IntegerField()
    estatura = models.FloatField()
    peso = models.FloatField()
    imc = models.FloatField()
    cintura = models.FloatField()
    sexo = models.IntegerField(choices=SEX_CHOICES)
    numero_hijos = models.IntegerField()
    diabetes_gestacional = models.BooleanField()
    diabetes = models.BooleanField()
    cigarrillos_semana = models.IntegerField()
    bebida_azucarada_semana = models.FloatField()
    deporte_semana = models.IntegerField()
    vasos_agua_semana = models.FloatField()
    sed = models.BooleanField()
    miccion_semana = models.IntegerField()
    antecedentes_familiares_glucosa = models.BooleanField()
    def __str__(self):
        return f"{self.fecha_hora} - {self.id}"

class Medida(models.Model):
    fecha_hora = models.DateTimeField()
    temperatura_dedo = models.FloatField()
    temperatura_externa = models.FloatField()
    senal_transmision = models.TextField()
    senal_reflexion = models.TextField()
    cuestionario = models.ForeignKey(Cuestionario, on_delete=models.CASCADE)
    def __str__(self):
        global TIPO_CHOICES
        return f"{self.fecha_hora} {TIPO_CHOICES[self.tipo]} - {self.id}"
