from muestras.models import Cuestionario, Medida
from muestras.serializers import CuestionarioSerializer
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from datetime import datetime
from django.utils import timezone

# rm -rf db.sqlite3
# rm -rf muestras/migrations/
# ./manage.py makemigrations muestras
# ./manage.py migrate
# ./manage.py shell

# https://docs.python.org/2/library/datetime.html#strftime-and-strptime-behavior
# fecha_hora = datetime.datetime(2013, 11, 20, 20, 8, 7, tz=timezone.utc)
fecha_hora = datetime.strptime('20-02-2019 23:40:00', '%d-%m-%Y %H:%M:%S').astimezone(timezone.get_current_timezone()) # .date()
cuestionario = Cuestionario(
        fecha_hora = fecha_hora,
        edad = 20,
        estatura = 1.70,
        peso = 50,
        imc = 35.18,
        cintura = 100,
        sexo = 1,
        numero_hijos = 2,
        diabetes_gestacional = 0,
        diabetes = 1,
        cigarrillos_semana = 0,
        bebida_azucarada_semana = 2,
        deporte_semana = 0,
        vasos_agua_semana = 70,
        sed = 1,
        miccion_semana = 100,
        antecedentes_familiares_glucosa = 1
)
cuestionario.save()

medida = Medida(
        fecha_hora = fecha_hora,
        senal_transmision = '1,2,3,4,5',
        senal_reflexion = '1,2,3,4,5',
        temperatura_dedo = 20,
        temperatura_externa = 19,
        cuestionario = cuestionario
)
medida.save()

from muestras.serializers import CuestionarioSerializer
serializer = CuestionarioSerializer(cuestionario)
content = JSONRenderer().render(serializer.data)
