# Iniciar virtualenv
```sh
cd env
source bin/activate
```

# Terminar virtualenv
```
deactivate
```

## Referencias
- https://rukbottoland.com/blog/tutorial-de-python-virtualenv/
- https://levipy.com/crear-api-rest-con-django-rest-framework/