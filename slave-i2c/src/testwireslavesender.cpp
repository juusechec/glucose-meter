// Wire Slave Sender
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Sends data as an I2C/TWI slave device
// Refer to the "Wire Master Reader" example for use with this

// Created 29 March 2006

// This example code is in the public domain.
// https://www.arduino.cc/en/Tutorial/MasterReader

#include <Arduino.h>
#include <Wire.h>

#define ADDR 0x40

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  Serial.println("Enviando...");
  Wire.write("hello "); // respond with message of 6 bytes
  // as expected by master
}

void setup() {
  Serial.begin(115200);
  Wire.begin(ADDR); // join i2c bus with address #8
  Wire.onRequest(requestEvent); // register event
}

void loop() {
  Serial.println("Cada 1seg");
  delay(1000);
}
